var langue=1;

function switchlanguage()
{
    if(langue==1)
    {
        langue=2;
    }
    else
    {
        langue=1;   
    }
    if(langue==1)
    {
        document.getElementById("jsaccueil").innerHTML="ACCUEIL";
        document.getElementById("jsprojets").innerHTML="PROJETS";
        // document.getElementById("jscv").innerHTML="CV";
        document.getElementById("welcome").innerHTML="Bienvenue !";
        document.getElementById("maskow").innerHTML="Je suis Rémi alias Maskow et vous voici sur mon site.";
        document.getElementById("presentation").innerHTML="Hey ! Je suis Rémi, un jeune développeur web <span>Front End</span> et <span>Back End</span> Français originaire des Hauts de France. Ce site me permet donc de vous présenter mes différents projets ainsi que mon CV.";
        document.getElementById("skill").innerHTML="COMPÉTENCES";
        document.getElementById("update").innerHTML="Date de mise à jour : <span>21/09/2022</span>";
        document.getElementById("texte1js").innerHTML="Création de page à l'aide des langage HTML, CSS, JavaScript, SQL, PHP. (Ruby prochainement)";
        document.getElementById("texte2js").innerHTML="Utilisation de logiciel Gratuit et Open Source de manière générale.";
        document.getElementById("texte3js").innerHTML="Utilisation de 'media queries' afin de rendre les pages responsive (adaptation aux différents support, smartphone, tablette...)";
    }
    if(langue==2)
    {
        document.getElementById("jsaccueil").innerHTML="HOME";
        document.getElementById("jsprojets").innerHTML="PROJECTS";
        // document.getElementById("jscv").innerHTML="RESUME";
        document.getElementById("welcome").innerHTML="Welcome !";
        document.getElementById("maskow").innerHTML="I'm Rémi AKA Maskow and here you are on my website.";
        document.getElementById("presentation").innerHTML="Hey, I'm Rémi, a young French <span>Front End</span> and <span>Back End</span> web developer, native of the Hauts de France. This site allows me to present my different projects and my resume.";
        document.getElementById("skill").innerHTML="SKILLS";
        document.getElementById("update").innerHTML="Date of update : <span>21/09/2022</span>";
        document.getElementById("texte1js").innerHTML="Page creation using HTML, CSS, JavaScript, SQL, PHP. (Ruby soon)";
        document.getElementById("texte2js").innerHTML="Use of Free and Open Source software in general.";
        document.getElementById("texte3js").innerHTML="Use of 'media queries' to make the pages responsive (adaptation to different media. smartphone, tablet...)";
    }
}




